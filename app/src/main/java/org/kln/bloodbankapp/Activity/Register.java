package org.kln.bloodbankapp.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;



import org.kln.bloodbankapp.R;

public class Register extends AppCompatActivity {
    private EditText nameEt,cityEt,bloodGroupEt,passwordEt,mobileEt;
    private Button submitButton;
    public static final String DB_NAME="Donor.db";
    DBHelper dbHelper;
    final static int Reqcode=123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        nameEt = findViewById(R.id.name);
        cityEt = findViewById(R.id.city);
        bloodGroupEt = findViewById(R.id.blood_group);
        passwordEt = findViewById(R.id.passward);
        mobileEt = findViewById(R.id.number);
        submitButton = findViewById(R.id.submit_button);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name, city, blood_group, password, mobile;
                name = nameEt.getText().toString();
                city = cityEt.getText().toString();
                blood_group = bloodGroupEt.getText().toString();
                password = passwordEt.getText().toString();
                mobile = mobileEt.getText().toString();
                showMessage(name + "\n" + city + "\n" + blood_group + "\n" + password + "\n" + mobile);

                dbHelper=new DBHelper(getApplicationContext(),DB_NAME,null,1);
                dbHelper.insertData(name,city,mobile,blood_group,password);

                navigateNext();

            }

        });


    }
    private void showMessage(String msg){
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }


    public void navigateNext(){
        Intent nextActivity=new Intent(this,MainActivity2.class);
        startActivityForResult(nextActivity,Reqcode);
    }


}