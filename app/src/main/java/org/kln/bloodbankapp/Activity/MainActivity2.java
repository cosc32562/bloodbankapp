package org.kln.bloodbankapp.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.kln.bloodbankapp.R;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
    }
    public void healthyTips(View view)
    {
        Ion.with(this)
                .load("https://health.gov/myhealthfinder/api/v3/itemlist.json?Type=topic")
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        Log.i("tip","Healthy tip is "+result);
                        displayTips(result);
                    }
                });
    }

    private void displayTips(String data)
    {
        try{
            JSONObject jsonObject= new JSONObject(data);
            JSONObject value=jsonObject.getJSONObject("value");
            String tip=value.getString("tip");
            TextView output=(TextView)findViewById(R.id.output);
            output.setText(tip);

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public void btnClickMaps(View view){
        Intent intent=new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("geo:47.4925,,19.0513"));
        Intent chooser=Intent.createChooser(intent,"Lounch Maps");
        startActivity(chooser);
    }

}