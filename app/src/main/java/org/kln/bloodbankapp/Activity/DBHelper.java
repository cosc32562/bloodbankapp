package org.kln.bloodbankapp.Activity;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DBHelper extends SQLiteOpenHelper {

    //creating the table Donor
    public static final String TABLE_NAME = "Donor";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TABLE_NAME + " ( name TEXT," +
                    "city TEXT, mobileNumber INTEGER,bloodGroup TEXT, password TEXT)";
    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TABLE_NAME;

    public DBHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);

    }
    //insert data to database
    public void insertData(String name, String city, String mobileNumber,String bloodGroup, String password) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("city", city);
        contentValues.put("mobileNumber", mobileNumber);
        contentValues.put("bloodGroup", bloodGroup);
        contentValues.put("password", password);
        long result = db.insert(TABLE_NAME, null, contentValues);

    }

}
